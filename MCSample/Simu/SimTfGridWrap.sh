#!/bin/bash
# /////////////////////////////////////////////////////////////////////
#
# This script needs an argument regarding to the number of skip events.
# When you use HTCondor, you can specify the argument via ~.sub file.
#
# /////////////////////////////////////////////////////////////////////

# You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
shopt -s expand_aliases

# set up the aliases.
# note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# now proceed normally to set up the other aliases
setupATLAS
asetup 21.0.68,Athena
voms-proxy-init -voms atlas
lsetup rucio
lsetup emi
lsetup panda

total_events=100000 # you can change the parameter as you like.
#simulator="MC12G4"
simulator="FullG4_LongLived"
inDS="user.ktakeda.SingleParticle.tau.multiple_subjobs.pT1000GeV.100000events.EVNT_EXT0/"
outDS="user.ktakeda.singletau.${simulator}.pT1000GeV.${total_events}events.HITS"

nFilesPerJob=1
nEventsPerFile=100
nEventsPerJob=10

pathena --trf "Sim_tf.py \
               --conditionsTag 'default:OFLCOND-MC16-SDR-14' \
               --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
               --physicsList 'FTFP_BERT_ATL' \
               --truthStrategy 'MC15aPlus' \
               --simulator ${simulator} \
               --DBRelease 'all:current' \
               --DataRunNumber '284500' \
               --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' \
               --postInclude 'default:mcHitRDO.py,RecJobTransforms/UseFrontier.py' \
               --maxEvents %MAXEVENTS \
               --skipEvents %SKIPEVENTS \
               --inputEVNTFile '%IN' \
               --outputHITSFile '%OUT.HITS.pool.root'" \
               --inDS=${inDS} \
               --outDS=${outDS} \
               --nEventsPerFile=${nEventsPerFile}\
               --nFilesPerJob=${nFilesPerJob} \
               --nEventsPerJob=${nEventsPerJob} 
