# MCSample
## Evgen
You can use these scripts to create evgen samples.
### how to submit a job
cd Evgen/
sh Generate.sh
### Description
pathena will scan InstallArea in your work directory. You should compile Generator packages before submitting a job.
Before compiling, you should configure the AthlasSoftware.
--> asetup AtlasProduction 17.2.0.2 64 here

how to compile:         
--> setupWorkArea.py    
--> cd WorkArea/cmt     
--> cmt config          
--> cmt bro gmake -j4   
--> cd \$TestArea       

After the above procedure, you can submit a transformation job to the grid system.
You'll see the InstallArea directory under the current your directory. The pathena will upload these directory.

## Simu
This directory includes a wrapper script of the Simu\_trf.py.
### how to submit a job
cd Simu/
sh SimTfGridWrap.sh

### Option files 
* mcHitRDO.py
  * This is to use the pixel information.

## Reco
This directory includes a wrapper script of the Reco\_trf.py.
### how to submit a job
cd Reco/
sh RecoTfGridWrap.sh
