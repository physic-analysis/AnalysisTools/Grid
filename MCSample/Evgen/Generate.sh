##!/bin/sh

echo " ******************************  Generate_trf.py  ***********************************"
echo "  pathena will scan InstallArea in your work directory. "
echo "  You should compile Generator packages before submitting a job."
echo "  Before compiling, you should configure the AthlasSoftware."
echo "  --> asetup AtlasProduction 17.2.0.2 64 here"
echo " "
echo "  how to compile:         "
echo "  --> setupWorkArea.py    "
echo "  --> cd WorkArea/cmt     "
echo "  --> cmt config          "
echo "  --> cmt bro gmake -j4   "
echo "  --> cd \$TestArea       "
echo " "
echo "  After the above procedure, you can submit a transformation job to the grid system."
echo " ***+++++*****************************************************************************"

total_events=100000 ## You can change this parameter as you like.
outputDS="user.ktakeda.SingleParticle.tau.multiple_subjobs.pT1000GeV.${total_events}events.EVNT"
maxEvents=100
jobConfig="single_tau_pT1000GeV.py"
nJobs=1000
nEventsPerJob=${maxEvents}
processed_events=`expr \( ${nJobs} \* ${nEventsPerJob} \) `

if [ "${processed_events}" -ne "${total_events}" ]; then 
  echo "***************** FATAL ERROR *****************"
  echo "You couldn't configure the parametes correctly."
  echo "You should consider the following parameters.  "
  echo " -- nJobs         = ${nJobs}"
  echo " -- nEventsPerJob = ${nEventsPerJob}"
  echo " -- total events  = ${total_number_of_events}"
  echo " "
  echo "Bye."
fi

# You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
shopt -s expand_aliases

# set up the aliases.
# note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
asetup AtlasProduction 17.2.0.2 64 here
#voms-proxy-init -voms atlas
lsetup rucio emi panda


pathena --trf "Generate_trf.py \
               ecmEnergy=13000 \
               runNumber=1 \
               randomSeed=%RNDM:12345 \
               firstEvent=1 \
               maxEvents=${maxEvents} \
               jobConfig=${jobConfig} \
               outputEVNTFile='%OUT.EVNT.pool.root'"\
        --athenaTag=17.2.0.2 \
        --nJobs=${nJobs} \
        --nEventsPerJob=${nEventsPerJob}\
        --outDS=${outputDS}
