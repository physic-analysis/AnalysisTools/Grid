
#  You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
shopt -s expand_aliases

# set up the aliases.
# note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
voms-proxy-init -voms atlas
lsetup rucio
lsetup emi
lsetup panda

while read line 
do
  echo $line
  INDS=$line
  EXEC="a.out"
  OUTDS="user.ktakeda.C05TriggerHole/"
  OUTPUTS="C05TriggerHole.root"
  WORKDIR=$(cd $(dirname $0); pwd)

  prun --exec  "echo %IN > GridInput.txt; ./${EXEC}" \
    --cmtConfig=x86_64-slc6-gcc49-opt \
    --nGBPerJob=MAX \
    --inDS ${INDS} \
    --outDS ${OUTDS} \
    --outputs ${OUTPUTS} \
    --workDir=${WORKDIR}  
done < $1
