#!/bin/sh

# check an argumet
if [ -z "$1" ]; then
  echo " ** WARNING **"
  echo "You should input availability.txt for the inDS argument."
  echo " *************"
  exit 1
fi

if [ -z "$2" ]; then
  echo " ** WARNING **"
  echo "You should input outDS version number."
  echo " *************"
  exit 1
fi
VERSION=$2

#  You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
shopt -s expand_aliases

# set up the aliases.
# note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
voms-proxy-init -voms atlas
lsetup rucio
lsetup emi
lsetup panda


counter=0
while read line 
do
  echo $line
  
  INDS=$line
  EXEC="mainGrid"
  OUTDS="user.ktakeda.C05TriggerHole.${VERSION}.${counter}/"
  OUTPUTS="L1TGCNtuple_2018.C05TriggerHole.root"
  WORKDIR=$(cd $(dirname $0); pwd)

  prun --exec "echo %IN>GridInput.txt;./${EXEC}" \
       --bexec "compileGrid.sh"\
       --cmtConfig=x86_64-slc6-gcc49-opt \
       --nGBPerJob=MAX \
       --inDS ${INDS} \
       --outDS ${OUTDS} \
       --outputs ${OUTPUTS} \
       --workDir=${WORKDIR} 
  
  counter=`expr $counter + 1`
done < $1
